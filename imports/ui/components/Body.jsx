import React from 'react';

import ChooseIndex from './ChooseIndex';

export default class Body extends React.Component{

  render(){
    return(
      <div className="body">
        <ChooseIndex/>
      </div>
    );
  };
};
