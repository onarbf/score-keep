import React from 'react';

export default class Header extends React.Component{

  render(){
    return(
      <div className="header">
        <div className="header-left">
          <h3>{this.props.title}</h3>
        </div>
        <div className="header-right">
          <h3>|||</h3>
        </div>
      </div>
    );
  };
};
